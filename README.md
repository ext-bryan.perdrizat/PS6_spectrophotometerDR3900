## Projet de semestre 6
### Mal de tête aux urgences, logiciel pour détecter une hémorragie cérébrale.

Ce répertoire contient les dossiers suivants

```
.
├── Code          	- Code du logiciel développé pour le projet
└── Documents     	- Toute la documentation concerant le projet
    ├── Algorithme	- Fichier de tableurs permettant de d'exécuter l'algorithme
    ├── Procédure 	- Les fichiers concernant la procédure de mesure, ainsi que les normes
    └── PV 	  	- PV de toutes les séances

```
